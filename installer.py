#!/usr/bin/env python


def install(alsi):
    alsi.sudo_apt_install([
        'python-hachoir-core',
        'python-hachoir-parser',
        'python-hachoir-metadata'
        ])

    alsi.info("Cleaver dependencies installed.")

if __name__ == '__main__':
    from assemblyline.al.install import SiteInstaller
    install(SiteInstaller())
